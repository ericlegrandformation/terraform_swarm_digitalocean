# CI/CD JENKINS dans le Cloud avec Terraform - Ansible - Docker Swarm

![a](https://miro.medium.com/max/800/1*LOFbTP2SxXcFpM_qTsUSuw.png)

Ce projet vise à mettre en service un pipeline CI/CD Jenkins dans le Cloud.

Pour cela, nous allons :
 * Provisionner et configurer une infrastructure CLOUD chez DigitalOcean avec Terraform, Ansible, Docker, Docker Swarm, Portainer 
 * Déployer une stack Jenkins sur notre infra
 * Configurer un pipeline CI/CD Jenkins 

## Pour commencer

Le fournisseur DigitalOcean permet d'obtenir un compte de démonstration GRATUIT pendant 30 jours grâce au code promo:  **ACTIVATE10** 

### Pré-requis

Ce qui est requis pour commencer avec ce projet :

- avoir créer un compte **Digital Ocean** et activer le code Promo:
[https://cloud.digitalocean.com/registrations/new?onboarding_origin=kubernetes](https://cloud.digitalocean.com/registrations/new?onboarding_origin=kubernetes)
- créer un nouveau projet appelé "JenkinsAsCode" par exemple
-  avoir réalisé la configuration de votre compte Digital Ocean, notamment,
créer le **Token** API Digital Ocean (menu API, click sur "Generate New Token") et la **clé ssh** pour accéder à Digital Ocean Cloud sans mot de passe (menu Security, click sur "add SSH Key" ): [https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean](https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean)

## Provisionner et configurer une infrastructure CLOUD chez DigitalOcean


Sur votre machine hôte, depuis votre répertoire **/home/'votre nom utilisateur'**, dans un terminal, taper successivement :
-     $ git clone https://framagit.org/ericlegrandformation/terraform_swarm_digitalocean.git
* ouvrer le dossier "terraform-swarm-digitalocean" **avec VSCode**
* ouvrer un **nouveau terminal** dans VSCode
* créer un environnement virtuel python avec la commande 
-     $ virtualenv --python=python3.5 venv
* activer l'environnement virtuel python avec la commande
-     $ source ./venv/bin/activate**"
* installer quelques pré-requis avec la commande 
-     $ sudo apt install libffi-dev python-dev libssl-dev
* installer ansible avec la commande
-     $ pip install ansible
* vérifier la version ansible (version 2.8 mini) avec
-     $ ansible --version

Procéder à l'installation de **Terraform**:

* Terraform n'est pas disponible sous forme de dépôt ubuntu/debian. Pour l'installer il faut le télécharger et l'installer manuellement:
-     $ cp /tmp
-     $ wget https://releases.hashicorp.com/terraform/0.12.6/terraform_0.12.6_linux_amd64.zip
-     $ sudo unzip ./terraform_0.12.6_linux_amd64.zip -d /usr/local/bin/
* puis tester l'installation:
-     $ terraform --version


Nous aurons aussi besoin d'un outil supplémentaire pour pouvoir faire un **inventaire dynamique**:
* suivez les instructions d'installation : [https://github.com/nbering/terraform-provider-ansible/](https://github.com/nbering/terraform-provider-ansible/)
 
Téléchargez également ce script qui servira effectivement d'inventaire:
-     $ wget https://raw.githubusercontent.com/nbering/terraform-inventory/master/terraform.py

Tous les outils sont installés, il faut procéder à la configuration maintenant. L'enregistrement du token et de la clé ssh dans le fichier terraform.tfvars:
* copiez le fichier  `terraform.tfvars.dist`  et renommez le en enlevant le  `.dist`
* rendez vous sur votre compte Digital Ocean pour récupérer le **token** et copiez le hash du token
* collez le token dans le fichier de variables  `terraform.tfvars` entre les guillemets de la ligne: do_api_token  = "ajouter_ici_token "
* récupérer la signature de votre **clé ssh** :
-     $ cd /home/"utilisateur"/.ssh
-     $ ssh-keygen -E md5 -lf ~/.ssh/id_rsa.pub | awk '{print $2}'
* et ajoutez le code retourné par la commande ci dessus (sans le MD5:), entre les guillemets de la ligne: do_sshkey_id  =  "ajouter_ici_sshkey "

Procéder à la création des serveurs:
- Lancer la commande :
-     $ cd /home/"utilisateur"/terraform_swarm/terraform_infra
-     $ terraform init
- Lancer la commande :
-     $ terraform apply
- Vérifier le bon fonctionnement des serveurs:
-     $ ansible swarm* -m ping
- Testez l'inventaire dynamique : 
-     $`chmod +x terraform.py && ./terraform.py`
Vous devriez avoir du texte JSON en retour de ce programme.

Terminer la configuration des serveurs en executant les **playbook Ansible**:
* Installer Swarm avec la commande
-     $ ansible-playbook install_swarm.yml

Vérifier la configuration des serveurs:
* Après avoir identifier les adresses ip de vos serveurs sur DigitalOcean, vérifier la configuration des 2 machines en visualisant tous les fichiers utiles après vous être connecter en ssh avec les commandes
-     $ ssh root@ip_serveur 

## Installer Portainer sur votre Swarm

Informations extraites de :
[https://portainer.readthedocs.io/en/stable/deployment.html](https://portainer.readthedocs.io/en/stable/deployment.html)
et
[https://github.com/portainer/portainer/issues/2057](https://github.com/portainer/portainer/issues/2057)


Vous connecter en ssh sur le manager avec les commandes
-     $ ssh root@ip_serveur_manager

Télécharger le fichier portainer-agent-stack.yml
-     $ curl -L https://downloads.portainer.io/portainer-agent-stack.yml -o portainer-agent-stack.yml

Corriger ce fichier, ajouter entre "environment:" et "# AGENT_PORT: 9001", les lignes suivantes:
-     # REQUIRED: Should be equal to the service name prefixed by "tasks." when
      # deployed inside an overlay network
      AGENT_CLUSTER_ADDR: tasks.agent

Déployer la stack portainer
-     $ docker stack deploy --compose-file=portainer-agent-stack.yml portainer

Visiter l'adresse: "ip_serveur_manager:9000", entrer un mot de passe pour votre utilisateur "admin" puis visualiser votre Swarm dans DashBoard, click sur "Go to cluster visualizer"

**Bravo, vous venez de provisionner et de configurer votre infrastructure dans le Cloud .**


## Déployer une stack Jenkins sur notre infra

Toujours depuis votre connection ssh sur votre serveur Swarm-manager
-     $ git clone https://framagit.org/ericlegrandformation/jenkins_as_code.git

Déployer la stack Jenkins
-     $ cd jenkins_as_code
-     $ docker stack deploy --compose-file=docker-stack.yml jenkins

Depuis portainer, visualiser votre Swarm dans DashBoard, click sur "Go to cluster visualizer", jenkins-master et jenkins-slave sont installer sur swarm-manager et swarm-worker.

Visiter l'adresse: "ip_serveur_manager:8080", entrer un mot de passe "jenkins" pour votre utilisateur "jenkins" puis entrer dans Jenkins.

**Bravo, vous venez d'installer Jenkins sur votre infrastructure dans le Cloud .**


## Configurer un pipeline CI/CD Jenkins

Depuis Jenkins, clicker sur "Open Blue Ocean"
- Clicker sur "Nouveau Pipeline"
- Clicker sur l'icone "Git"
- Remplir "Repository URL", "Username" et "Password"
- Clicker sur "Create Credential" puis "Create Pipeline"

**Bravo, vous venez de créer un pipeline Jenkins pour votre application.**


## Versions

**Dernière version stable :** 0.1
**Dernière version :** 0.1
Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/terraform_swarm_digitalocean.git)

## Auteurs

* **Eric Legrand** _alias_ [@ericlegrand](https://framagit.org/ericlegrandformation/terraform_swarm_digitalocean.git)
avec le soutien de Messieurs Hugh Norris et Elie Gavoty.
